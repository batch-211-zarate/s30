
// s30 - Acticvity Solution

// Results of using MongoDB Aggregation to count the total number of fruits on sale.

	db.fruits.aggregate([
    	{$match: {onSale:true}},      
    	{$count: "fruitsOnSale"}
	]);

// Results of using MongoDB Aggregation to count the total number of fruits with stock more than 20.

	db.fruits.aggregate([
    	{$match: {stock: { $gte: 20}}},
    	{$count: "enoughStock"}		
	]);


// Results of using MongoDB Aggregation to get the average price of fruits on sale per supplier.

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
		{$sort: {min_price: 1}}
	]);

// Results of using MongoDB Aggregation to get the highest price of a fruit per supplier

	db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
		{$sort: {max_price: -1}}
	]);

// Results of using MongoDB Aggregation to get the lowest price of a fruit per supplier
	
	db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
		{$sort: {min_price: 1}}
	]);