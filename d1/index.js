
// Aggregation in MongoDB
	/*
		Overview
			Aggregation
				- Definition

			Aggregate Method
				1. Match
				2. Group
				3. Sort
				4. Project

			Aggregation Pipeline

			Operators
				1. Sum
				2. Max
				3. Min
				4. Avg
	*/


// MongoDB Aggregation
	/*
		- Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
		-  Compared to doing CRUD Operations on our data from our previous sessions, aggregation gives us access to manipulate, filter, and compute results, providing us with information to make a necessary development decisions.
		- Aggregations in MongoDB are very flexible and you can form your own aggregation pipeline depending on the nedd of your application.
	*/


// Aggregate Methods
	
	// 1. Match : "$match"
		/*
			- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process

			Syntax:
				{ $match: {field: valaue}}
		*/
			db.fruits.aggregate([
				{ $match: {onSale: true}}
			]);

			db.fruits.aggregate([
				{ $match: {stock: { $gte: 20}}}
			]);


	// 2. Group : "$group"
		/*
			- The "$group" is used to group elements together field-value pairs using the data from the grouped element.

			Syntax:
				{ $group: {_id: "value", fieldResult: "valueResult"}}
		*/ 

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}	
			]);


	// 3. Sort : "$sort"
		/*
			- The "$sort" can be used when changing the order aggregated results
			- Providing a value of -1 will sort the documents in a descending order
			- Providing a value of 1 will sort the documents in a ascending order

			Syntax:
				{ $sort: {field: -1/1}}
		*/

			db.fruits.aggregate([
				{ $match: { onSale: true} },
				{ $group: { _id: "$supplier_id", total: {$sum: "$stock"} } },
				{ $sort: {total: 1} }
			]);


	// 4. Project : "$project"
		/*
			- The "$project" can be used when aggregating data to include or exclude fields from the returned results

			Syntax:
				{$project: {field: 1/10}}
		*/

			db.fruits.aggregate([
				{ $match: { onSale: true}},
				{ $group:{_id: "$supplier_id", total: {$sum: "$stock"}}},
				{ $project: {_id:0}}
			]);

			db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
				{$project: {_id: 0}},
				{$sort: {total: -1}}
			]);


// Aggregation Pipelines
	/*
		- The aggregation pipeline in MongoDB is a framework for data aggregation
		- Each stage transforms the documents as they pass throught the deadlines

		Syntax:
			db.collectionName.aggregate([
				{stageA},
				{stageB},
				{stageC}
			]);
	*/


// Operators
	
	// 1. Sum : "$sum"
		// gets the total of everything

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
			]);


	// 2. Max : "$max"
		// gets the highest value out of everything else

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
			]);


	// 3. Min : "$min"
		// gets the lowest  value out of everyting else

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}	
			]);


	// 4. Average : "#avg"
		// gets the average value of all the fields

			db.fruits.aggregate([
				{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
			]);